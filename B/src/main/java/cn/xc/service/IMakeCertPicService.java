package cn.xc.service;

import java.io.OutputStream;

/**
 * 验证码生成服务接口
 *
 */
public interface IMakeCertPicService {
    String getCerPic(int width, int height, OutputStream os);
}
