package cn.xc.constant;

/**
 * 登录相关常量类
 */
public final class LoginConstant {
    public static final String USER_TYPE_ADMIN = "1";
    public static final String USER_TYPE_CUSTOM_USER = "0";
    public static final String PARAMETER_USER_TYPE = "userType";
    public static final String PARAMETER_USERNAME = "username";
    public static final String PARAMETER_PASSWORD = "password";
    public static final String PARAMETER_CAPTCHA = "certCode";
    public static final String SESSION_USER_ID = "userID";
    public static final String SESSION_USER_TYPE = "userType";


}
