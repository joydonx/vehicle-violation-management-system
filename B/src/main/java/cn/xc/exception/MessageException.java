package cn.xc.exception;

/**
 *  私信服务异常类
 */
public class MessageException extends Exception {
    public MessageException(String message) {
        super(message);
    }
}
