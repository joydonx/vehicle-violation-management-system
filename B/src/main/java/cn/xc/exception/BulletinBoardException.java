package cn.xc.exception;

/**
 *  DAO类空指针异常类
 */
public class BulletinBoardException extends Exception {

    public BulletinBoardException(String message) {
        super(message);
    }
}
