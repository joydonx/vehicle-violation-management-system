package cn.xc.exception;

/**
 *  车辆违章异常类
 */
public class ViolatingInformationException extends Exception {
    public ViolatingInformationException(String message) {
        super(message);
    }
}
