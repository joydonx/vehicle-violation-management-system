package cn.xc.exception;

/**
 *  咨询单服务异常类
 */
public class ConsultationListException extends Exception {
    public ConsultationListException(String message) {
        super(message);
    }
}
