package cn.xc.exception;

/**
 *  管理员服务异常类
 */
public class AdminException extends Exception {
    public AdminException(String message) {
        super(message);
    }
}
