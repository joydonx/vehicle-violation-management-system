package cn.xc.exception;

/**
 *  用户信息服务异常类.
 */
public class UserException extends Exception {
    public UserException(String message) {
        super(message);
    }
}
