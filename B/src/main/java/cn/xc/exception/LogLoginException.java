package cn.xc.exception;

/**
 *  登录日志服务异常类
 */
public class LogLoginException extends Exception {
    public LogLoginException(String message) {
        super(message);
    }
}
