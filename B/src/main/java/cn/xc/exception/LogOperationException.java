package cn.xc.exception;

/**
 *  操作日志服务异常类
 */
public class LogOperationException extends Exception {
    public LogOperationException(String message) {
        super(message);
    }
}
