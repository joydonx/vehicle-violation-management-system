package cn.xc.dao;

import cn.xc.dao.condition.ViolatingInformationExample;
import cn.xc.entity.DO.ViolatingInformationDO;
import cn.xc.service.impl.ViolatingInformationServiceImpl;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 违章信息数据表操作接口
 */
public interface IViolatingInformationDAO {
    long countByExample(ViolatingInformationExample example);

    int deleteByExample(ViolatingInformationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ViolatingInformationDO record);

    int insertSelective(ViolatingInformationDO record);

    List<ViolatingInformationDO> selectByExample(ViolatingInformationExample example);
    List<ViolatingInformationDO> selectByCarNumber(cn.xc.entity.DTO.ViolatingQueryDTO x);

    ViolatingInformationDO selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ViolatingInformationDO record, @Param("example") ViolatingInformationExample example);

    int updateByExample(@Param("record") ViolatingInformationDO record, @Param("example") ViolatingInformationExample example);

    int updateByPrimaryKeySelective(ViolatingInformationDO record);

    int updateByPrimaryKey(ViolatingInformationDO record);

    void deleteByPrimaryKeyList(List<Long> primaryKey);


}
