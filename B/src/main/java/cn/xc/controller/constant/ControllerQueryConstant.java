package cn.xc.controller.constant;

/**
 * 违章信息控制层常量类
 */
public class ControllerQueryConstant {
    public static final String QUERY_PARAM_SCOPE_START = "start";
    public static final String QUERY_PARAM_SCOPE_END = "end";
    public static final String QUERY_SINGLE_PARAM = "param";

}
