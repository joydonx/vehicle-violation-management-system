package cn.xc.entity.DTO;

/**
 * 违章情况查询DTO
 * - carNumber
 */
public class ViolatingQueryDTO {
    private String carNumber;

    public ViolatingQueryDTO() {
    }

    public ViolatingQueryDTO(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }
}
