/*
 Navicat Premium Data Transfer

 Source Server         : XQY
 Source Server Type    : MySQL
 Source Server Version : 80025
 Source Host           : localhost:3306
 Source Schema         : vehiclemanagement

 Target Server Type    : MySQL
 Target Server Version : 80025
 File Encoding         : 65001

 Date: 11/06/2021 09:42:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` tinyint(0) NOT NULL,
  `admin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` tinyint(0) NOT NULL,
  `phone_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `admin_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, '1001', 1, '李强', 1, '12345678923', '49c2435d-3c8a-4b18-b199-85b31362d08b.png', NULL, '12345678@qq,com', '123456', '2021-06-11 09:38:16', '2021-06-11 09:38:16', 0);

-- ----------------------------
-- Table structure for bulletin_board
-- ----------------------------
DROP TABLE IF EXISTS `bulletin_board`;
CREATE TABLE `bulletin_board`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `publisher_id` bigint(0) NOT NULL,
  `context` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `type` tinyint(0) NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 62 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bulletin_board
-- ----------------------------
INSERT INTO `bulletin_board` VALUES (64, 1, 'A\nAAAA', 2, '2021-06-10 23:41:24', '2021-06-10 23:37:37', 0, 'A');
INSERT INTO `bulletin_board` VALUES (65, 1, 'B BBBB\n', 2, '2021-06-11 00:56:36', '2021-06-11 00:56:36', 0, '标题B');
INSERT INTO `bulletin_board` VALUES (66, 1, 'SSS-HELLO\n', 3, '2021-06-11 00:50:14', '2021-06-11 00:50:14', 1, 'SS');
INSERT INTO `bulletin_board` VALUES (67, 1, 'XXXxxx\n', 2, '2021-06-11 09:36:26', '2021-06-11 09:36:26', 1, 'X');

-- ----------------------------
-- Table structure for car_information
-- ----------------------------
DROP TABLE IF EXISTS `car_information`;
CREATE TABLE `car_information`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `car_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `number_violation` int(0) NOT NULL,
  `owner` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `engine_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `frame_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `gmt_modifier` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`id`, `car_number`) USING BTREE,
  INDEX `car_number`(`car_number`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of car_information
-- ----------------------------
INSERT INTO `car_information` VALUES (1, '皖E00001', 1, '徐启远', '20210505', '20210506', '2021-06-09 17:24:30', '2021-05-31 08:48:59', 0);

-- ----------------------------
-- Table structure for consultation_list
-- ----------------------------
DROP TABLE IF EXISTS `consultation_list`;
CREATE TABLE `consultation_list`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `type` tinyint(0) NOT NULL,
  `identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `creater_id` bigint(0) NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `context` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `processor_id` bigint(0) NOT NULL,
  `response_context` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `response_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `status` tinyint(0) NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of consultation_list
-- ----------------------------

-- ----------------------------
-- Table structure for log_login
-- ----------------------------
DROP TABLE IF EXISTS `log_login`;
CREATE TABLE `log_login`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `login_user_id` bigint(0) NOT NULL,
  `login_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `login_user_identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_admin` tinyint(0) NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of log_login
-- ----------------------------
INSERT INTO `log_login` VALUES (1, 1, '127.0.0.1', '2021-05-29 18:31:03', '1001', 1, '2021-05-29 18:31:03', '2021-05-29 18:31:03', 0);
INSERT INTO `log_login` VALUES (2, 1, '0:0:0:0:0:0:0:1', '2021-05-29 21:14:18', '1001', 1, '2021-05-29 21:14:18', '2021-05-29 21:14:18', 0);
INSERT INTO `log_login` VALUES (3, 1, '127.0.0.1', '2021-05-30 02:39:13', '1001', 1, '2021-05-30 02:39:13', '2021-05-30 02:39:13', 0);
INSERT INTO `log_login` VALUES (4, 1, '127.0.0.1', '2021-05-31 09:37:37', '179289', 0, '2021-05-31 09:37:37', '2021-05-31 09:37:37', 0);
INSERT INTO `log_login` VALUES (5, 1, '127.0.0.1', '2021-05-31 11:09:01', '1001', 1, '2021-05-31 11:09:01', '2021-05-31 11:09:01', 0);
INSERT INTO `log_login` VALUES (6, 1, '127.0.0.1', '2021-06-02 10:00:19', '1001', 1, '2021-06-02 10:00:19', '2021-06-02 10:00:19', 0);
INSERT INTO `log_login` VALUES (7, 1, '127.0.0.1', '2021-06-02 11:22:58', '1001', 1, '2021-06-02 11:22:58', '2021-06-02 11:22:58', 0);
INSERT INTO `log_login` VALUES (8, 1, '127.0.0.1', '2021-06-04 09:40:05', '1001', 1, '2021-06-04 09:40:05', '2021-06-04 09:40:05', 0);
INSERT INTO `log_login` VALUES (9, 1, '0:0:0:0:0:0:0:1', '2021-06-06 15:18:51', '1001', 1, '2021-06-06 15:18:52', '2021-06-06 15:18:52', 0);
INSERT INTO `log_login` VALUES (10, 1, '0:0:0:0:0:0:0:1', '2021-06-06 15:29:32', '1001', 1, '2021-06-06 15:29:32', '2021-06-06 15:29:32', 0);
INSERT INTO `log_login` VALUES (11, 1, '127.0.0.1', '2021-06-06 15:43:24', '1001', 1, '2021-06-06 15:43:24', '2021-06-06 15:43:24', 0);
INSERT INTO `log_login` VALUES (12, 1, '0:0:0:0:0:0:0:1', '2021-06-06 15:46:09', '179289', 0, '2021-06-06 15:46:09', '2021-06-06 15:46:09', 0);
INSERT INTO `log_login` VALUES (13, 1, '0:0:0:0:0:0:0:1', '2021-06-06 16:13:20', '1001', 1, '2021-06-06 16:13:20', '2021-06-06 16:13:20', 0);
INSERT INTO `log_login` VALUES (14, 1, '0:0:0:0:0:0:0:1', '2021-06-08 16:02:08', '179289', 0, '2021-06-08 16:02:08', '2021-06-08 16:02:08', 0);
INSERT INTO `log_login` VALUES (15, 1, '0:0:0:0:0:0:0:1', '2021-06-08 17:24:13', '1001', 1, '2021-06-08 17:24:13', '2021-06-08 17:24:13', 0);
INSERT INTO `log_login` VALUES (16, 1, '0:0:0:0:0:0:0:1', '2021-06-08 17:24:49', '1001', 1, '2021-06-08 17:24:49', '2021-06-08 17:24:49', 0);
INSERT INTO `log_login` VALUES (17, 1, '0:0:0:0:0:0:0:1', '2021-06-08 19:17:25', '179289', 0, '2021-06-08 19:17:25', '2021-06-08 19:17:25', 0);
INSERT INTO `log_login` VALUES (18, 1, '0:0:0:0:0:0:0:1', '2021-06-08 20:06:38', '1001', 1, '2021-06-08 20:06:38', '2021-06-08 20:06:38', 0);
INSERT INTO `log_login` VALUES (19, 1, '0:0:0:0:0:0:0:1', '2021-06-08 21:52:07', '179289', 0, '2021-06-08 21:52:07', '2021-06-08 21:52:07', 0);
INSERT INTO `log_login` VALUES (20, 1, '0:0:0:0:0:0:0:1', '2021-06-08 22:00:21', '179289', 0, '2021-06-08 22:00:21', '2021-06-08 22:00:21', 0);
INSERT INTO `log_login` VALUES (21, 1, '0:0:0:0:0:0:0:1', '2021-06-09 11:33:05', '1001', 1, '2021-06-09 11:33:05', '2021-06-09 11:33:05', 0);
INSERT INTO `log_login` VALUES (22, 1, '0:0:0:0:0:0:0:1', '2021-06-09 15:43:14', '179289', 0, '2021-06-09 15:43:14', '2021-06-09 15:43:14', 0);
INSERT INTO `log_login` VALUES (23, 1, '0:0:0:0:0:0:0:1', '2021-06-09 15:47:43', '1001', 1, '2021-06-09 15:47:43', '2021-06-09 15:47:43', 0);
INSERT INTO `log_login` VALUES (24, 1, '0:0:0:0:0:0:0:1', '2021-06-10 13:47:47', '179289', 0, '2021-06-10 13:47:47', '2021-06-10 13:47:47', 0);
INSERT INTO `log_login` VALUES (25, 1, '0:0:0:0:0:0:0:1', '2021-06-10 13:55:21', '179289', 0, '2021-06-10 13:55:21', '2021-06-10 13:55:21', 0);
INSERT INTO `log_login` VALUES (26, 1, '0:0:0:0:0:0:0:1', '2021-06-10 14:29:17', '1001', 1, '2021-06-10 14:29:18', '2021-06-10 14:29:18', 0);
INSERT INTO `log_login` VALUES (27, 1, '0:0:0:0:0:0:0:1', '2021-06-10 17:14:01', '179289', 0, '2021-06-10 17:14:01', '2021-06-10 17:14:01', 0);
INSERT INTO `log_login` VALUES (28, 1, '0:0:0:0:0:0:0:1', '2021-06-10 10:38:51', '1001', 1, '2021-06-10 18:38:51', '2021-06-10 18:38:51', 0);
INSERT INTO `log_login` VALUES (29, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:02:53', '1001', 1, '2021-06-10 19:02:52', '2021-06-10 19:02:52', 0);
INSERT INTO `log_login` VALUES (30, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:03:05', '1001', 1, '2021-06-10 19:03:04', '2021-06-10 19:03:04', 0);
INSERT INTO `log_login` VALUES (31, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:30:47', '1001', 1, '2021-06-10 19:30:47', '2021-06-10 19:30:47', 0);
INSERT INTO `log_login` VALUES (32, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:32:46', '1001', 1, '2021-06-10 19:32:46', '2021-06-10 19:32:46', 0);
INSERT INTO `log_login` VALUES (33, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:33:15', '1001', 1, '2021-06-10 19:33:15', '2021-06-10 19:33:15', 0);
INSERT INTO `log_login` VALUES (34, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:43:13', '1001', 1, '2021-06-10 19:43:12', '2021-06-10 19:43:12', 0);
INSERT INTO `log_login` VALUES (35, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:44:18', '1001', 1, '2021-06-10 19:44:17', '2021-06-10 19:44:17', 0);
INSERT INTO `log_login` VALUES (36, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:55:06', '1001', 1, '2021-06-10 19:55:06', '2021-06-10 19:55:06', 0);
INSERT INTO `log_login` VALUES (37, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:55:45', '1001', 1, '2021-06-10 19:55:45', '2021-06-10 19:55:45', 0);
INSERT INTO `log_login` VALUES (38, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:58:01', '1001', 1, '2021-06-10 19:58:00', '2021-06-10 19:58:00', 0);
INSERT INTO `log_login` VALUES (39, 1, '0:0:0:0:0:0:0:1', '2021-06-10 11:58:19', '1001', 1, '2021-06-10 19:58:18', '2021-06-10 19:58:18', 0);
INSERT INTO `log_login` VALUES (40, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:04:27', '1001', 1, '2021-06-10 20:04:26', '2021-06-10 20:04:26', 0);
INSERT INTO `log_login` VALUES (41, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:05:24', '1001', 1, '2021-06-10 20:05:24', '2021-06-10 20:05:24', 0);
INSERT INTO `log_login` VALUES (42, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:12:53', '1001', 1, '2021-06-10 20:12:53', '2021-06-10 20:12:53', 0);
INSERT INTO `log_login` VALUES (43, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:12:53', '1001', 1, '2021-06-10 20:12:53', '2021-06-10 20:12:53', 0);
INSERT INTO `log_login` VALUES (44, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:14:51', '1001', 1, '2021-06-10 20:14:50', '2021-06-10 20:14:50', 0);
INSERT INTO `log_login` VALUES (45, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:15:43', '1001', 1, '2021-06-10 20:15:42', '2021-06-10 20:15:42', 0);
INSERT INTO `log_login` VALUES (46, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:36:22', '1001', 1, '2021-06-10 20:36:22', '2021-06-10 20:36:22', 0);
INSERT INTO `log_login` VALUES (47, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:38:11', '1001', 1, '2021-06-10 20:38:11', '2021-06-10 20:38:11', 0);
INSERT INTO `log_login` VALUES (48, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:39:02', '1001', 1, '2021-06-10 20:39:02', '2021-06-10 20:39:02', 0);
INSERT INTO `log_login` VALUES (49, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:46:48', '1001', 1, '2021-06-10 20:46:48', '2021-06-10 20:46:48', 0);
INSERT INTO `log_login` VALUES (50, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:46:50', '1001', 1, '2021-06-10 20:46:50', '2021-06-10 20:46:50', 0);
INSERT INTO `log_login` VALUES (51, 1, '0:0:0:0:0:0:0:1', '2021-06-10 12:47:02', '1001', 1, '2021-06-10 20:47:01', '2021-06-10 20:47:01', 0);
INSERT INTO `log_login` VALUES (52, 1, '0:0:0:0:0:0:0:1', '2021-06-10 13:49:18', '1001', 1, '2021-06-10 21:49:17', '2021-06-10 21:49:17', 0);
INSERT INTO `log_login` VALUES (53, 1, '0:0:0:0:0:0:0:1', '2021-06-10 14:02:07', '1001', 1, '2021-06-10 22:02:07', '2021-06-10 22:02:07', 0);
INSERT INTO `log_login` VALUES (54, 1, '0:0:0:0:0:0:0:1', '2021-06-10 14:14:03', '1001', 1, '2021-06-10 22:14:03', '2021-06-10 22:14:03', 0);
INSERT INTO `log_login` VALUES (55, 1, '0:0:0:0:0:0:0:1', '2021-06-10 15:06:15', '1001', 1, '2021-06-10 23:06:15', '2021-06-10 23:06:15', 0);
INSERT INTO `log_login` VALUES (56, 1, '0:0:0:0:0:0:0:1', '2021-06-10 15:25:32', '1001', 1, '2021-06-10 23:25:32', '2021-06-10 23:25:32', 0);
INSERT INTO `log_login` VALUES (57, 1, '0:0:0:0:0:0:0:1', '2021-06-10 15:26:14', '1001', 1, '2021-06-10 23:26:14', '2021-06-10 23:26:14', 0);
INSERT INTO `log_login` VALUES (58, 1, '0:0:0:0:0:0:0:1', '2021-06-10 17:12:38', '179289', 0, '2021-06-11 01:12:37', '2021-06-11 01:12:37', 0);
INSERT INTO `log_login` VALUES (59, 1, '0:0:0:0:0:0:0:1', '2021-06-10 17:13:22', '1001', 1, '2021-06-11 01:13:22', '2021-06-11 01:13:22', 0);
INSERT INTO `log_login` VALUES (60, 1, '0:0:0:0:0:0:0:1', '2021-06-11 01:28:09', '1001', 1, '2021-06-11 09:28:09', '2021-06-11 09:28:09', 0);

-- ----------------------------
-- Table structure for log_operation
-- ----------------------------
DROP TABLE IF EXISTS `log_operation`;
CREATE TABLE `log_operation`  (
  `id` bigint(0) NOT NULL AUTO_INCREMENT,
  `op_user_id` bigint(0) NOT NULL,
  `op_user_identifier` bigint(0) NOT NULL,
  `op_ip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `op_type` tinyint(0) NOT NULL,
  `op_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `op_context` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of log_operation
-- ----------------------------

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` bigint(0) NOT NULL,
  `sender_id` bigint(0) NOT NULL,
  `receiver_id` bigint(0) NOT NULL,
  `context` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_read` tinyint(0) NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`sender_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` bigint(0) NOT NULL,
  `identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `nick_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sex` tinyint(0) NOT NULL,
  `phone_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `idcard_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `car_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `birth_date` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`identifier`) USING BTREE,
  INDEX `car_number`(`car_number`) USING BTREE,
  CONSTRAINT `car_number` FOREIGN KEY (`car_number`) REFERENCES `car_information` (`car_number`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, '179289', '徐启远', 'joydon', 2, '123456789', '111110', '皖E00001', '11111', '28b3eab3-dd51-49d9-b149-af061544aecc.png', '11', '111222', '2021-06-09 17:23:59', '2021-05-12 09:36:44', '2021-06-11 01:12:52', 0);

-- ----------------------------
-- Table structure for user_point
-- ----------------------------
DROP TABLE IF EXISTS `user_point`;
CREATE TABLE `user_point`  (
  `id` bigint(0) NOT NULL,
  `identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `current_point` tinyint(0) NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `gmt_modifier` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`identifier`) USING BTREE,
  CONSTRAINT `identifier` FOREIGN KEY (`identifier`) REFERENCES `user` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of user_point
-- ----------------------------
INSERT INTO `user_point` VALUES (1, '179289', 11, '2021-06-07 21:53:04', '2021-06-08 21:53:20', 0);

-- ----------------------------
-- Table structure for violation_information
-- ----------------------------
DROP TABLE IF EXISTS `violation_information`;
CREATE TABLE `violation_information`  (
  `id` bigint(0) NOT NULL,
  `identifier` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `car_type` tinyint(0) NOT NULL,
  `violation_type` tinyint(0) NOT NULL,
  `violation_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0),
  `detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `car_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `penalty_point` tinyint(0) NOT NULL,
  `penalty_money` decimal(65, 0) NOT NULL,
  `gmt_create` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `gmt_modified` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0),
  `is_deleted` tinyint(0) NOT NULL,
  PRIMARY KEY (`identifier`) USING BTREE,
  CONSTRAINT `identifer` FOREIGN KEY (`identifier`) REFERENCES `user` (`identifier`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of violation_information
-- ----------------------------
INSERT INTO `violation_information` VALUES (1, '179289', 2, 1, '2021-06-09 17:24:49', '闯红灯', '皖E00001', 3, 250, '2021-05-05 09:01:01', '2021-06-11 09:37:27', 0);

SET FOREIGN_KEY_CHECKS = 1;
