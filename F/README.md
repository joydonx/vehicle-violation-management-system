## 前端配置

- 当前系统NPM版本
- 安装依赖
- 启动

---

#### 一：当前系统NPM版本

 `npm install npm@6.14.10 -g `



#### 二：安装依赖

`npm i`



#### 三：启动

`npm run dev`