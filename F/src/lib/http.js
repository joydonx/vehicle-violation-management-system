/* 封装Axios基本请求 */

import Axios from 'axios';


export const baseUrl = 'http://localhost:8080';

let axios = Axios.create({
    baseURL: baseUrl,
    timeout: 30000,
    headers: {
        'Content-Type': 'application/json; charset=utf-8'
    }
});

/* start: 基本get与post携带数据与后台交互 */
export const getAxios = (url, data) => {
    return axios.get(url, {
        params: data
    });
};
export const postAxios = (url, data) => {
    return axios.post(url, data);
};
/* end: 基本get与post携带数据与后台交互 */

/* start: 文件post相关，post上传、get无参下载、post下载 */
export const postFileUploadAxios = (url, data) => {
    return axios.post(url, data, { headers: { 'Content-Type': 'multipart/form-data' } });
};
export const getDownloadAxios = (url) => {
    return axios.get(url, { responseType: 'blob' });
};
export const postDownloadAxios = (url, data) => {
    return axios.post(url, data, { responseType: 'blob' });
};
/* end: 下载请求，文件post相关，post上传、get下载、post下载  */

// /**
//  * 请求拦截器：在请求发送之前，向请求头中添加token
//  * config ->
//  *   config.headers['xx']
//  */
// axios.interceptors.request.use(
//     function (config) {
//         if (cookie.getToken()) {
//             config.headers['x-access-token'] = cookie.getToken();
//         }
//         return config;
//     },
//     function (error) {
//         Spin.hide();
//         return Promise.reject(error);
//     }
// );
//
// /**
//  * 响应了拦截器（在响应之后对数据进行一些处理）
//  * res ->
//  *   res.config.responseType
//  *   res.headers['content-type']
//  *   res.data.code
//  *   res.data.msg
//  */
// axios.interceptors.response.use(
//     res => {
//         if (res.config.responseType === 'blob') {
//             // 有headers请求头、有content-type、content-type为json属性。
//             let isReturnJson = res.headers && res.headers['content-type'] && res.headers['content-type'].indexOf('json') > -1;
//             // 错误数据处理（在相应类型为blob的情况下收到了application/json类型数据）。
//             if (isReturnJson) {
//                 // 使用FileReader（DOM中的文件存取API）: https://developer.mozilla.org/zh-CN/docs/Web/API/FileReader。
//                 let reader = new FileReader();
//                 // 读取完成时触发
//                 reader.onload = function (event) {
//                     // 将读取到的JSON字符串转换为json格式数据。
//                     return validateResponseCode({
//                         data: JSON.parse(reader.result)
//                     });
//                 };
//                 // 开始读取指定的Blob中的内容。一旦完成，result属性中将包含一个字符串以表示所读取的文件内容。
//                 reader.readAsText(res.data);
//                 return true;
//             } else {
//                 // 下载文件。
//                 download(res);
//             }
//         } else {
//             // 正常json请求。
//             return validateResponseCode(res);
//         }
//     },
//     error => {
//         Spin.hide();
//         Message.error('服务内部错误，AXIOS响应拦截捕获到异常！');
//         return Promise.reject(error);
//     }
// );
//
// /**
//  * 验证后端响应码
//  *   [ data.code + data.msg ]
//  *   [ data.code分为三种情况考虑，1001,502,其他 ]
//  * @param res
//  * @returns {Promise<never>|Promise<unknown>}
//  */
// function validateResponseCode (res) {
//     let { data } = res;
//     // 系统常量{"code":1,"msg":"操作成功"}
//     if (data && data.code && data.code !== 1) {
//         // 1001，登录异常常量：[登录异常]
//         if (data.code === 1001) {
//             cookie.clearToken();
//             localStorage.clear();
//             window.location.href = window.location.pathname + '#/login';
//             Message.error('未登录，或登录失效，请登录');
//             return;
//         } else if (data.code === 502) {
//             window.location.href = window.location.pathname + '#/500';
//             return;
//         } else {
//             Spin.hide();
//             Message.error(data.msg);
//             return Promise.reject(res);
//         }
//     }
//     return Promise.resolve(data);
// }
//
// /* start：下载 */
//
// /**
//  * 下载操作(准备)
//  * @param res
//  */
// function download (res) {
//     let reader = new FileReader();
//     let data = res.data;
//     reader.onload = e => {
//         // 如果读取失败，e.target.result中将不会出现Result，且e.target.result将不会包含Result属性
//         // 注意Result属性首字母大写
//         if (e.target.result.indexOf('Result') !== -1 && JSON.parse(e.target.result).Result === false) {
//             // 进行错误处理。
//         } else {
//             let fileName = 'download';
//             // 读取文件名：https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Headers/Content-Disposition。
//             // Content-Disposition: attachment; filename="filename.jpg"
//             let contentDisposition = res.headers['Content-Disposition'];
//             contentDisposition = contentDisposition ? contentDisposition : res.headers['content-disposition'];
//             if (contentDisposition) {
//                 fileName = window.decodeURI(contentDisposition.split('=')[1]);
//             }
//             executeDownload(data, fileName);
//         }
//     };
//     reader.readAsText(data);
// }
//
// /**
//  * 模拟创建a标签点击，执行下载操作
//  * @param data
//  * @param fileName
//  */
// function executeDownload (data, fileName) {
//     if (!data) {
//         return;
//     }
//     let url = window.URL.createObjectURL(new Blob([data]));
//     let link = document.createElement('a');
//     link.style.display = 'none';
//     link.href = url;
//     link.setAttribute('download', fileName);
//     document.body.appendChild(link);
//     link.click();
//     document.body.removeChild(link);
// }
//
// /* end：下载 */
